﻿using System;

namespace Catalog.DataContracts
{
    public class Person
    {
        public Person()
        {
            Id = Guid.NewGuid();
        }
        public Guid Id { get; set; }
        public string Name { get; set; }

        public Guid TeamId { get; set; }
        public Team Team { get; set; }
    }
}
