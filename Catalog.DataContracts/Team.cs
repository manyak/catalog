﻿using System;

namespace Catalog.DataContracts
{
    public class Team
    {
        public Team()
        {
            Id = Guid.NewGuid();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
