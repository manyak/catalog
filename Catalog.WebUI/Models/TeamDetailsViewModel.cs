﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Catalog.DataContracts;

namespace Catalog.WebUI.Models
{
    public class TeamDetailsViewModel
    {
        public Team Team { get; set; }
        public IEnumerable<Person> TeamMembers { get; set; }
    }
}