﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Catalog.DataContracts;

namespace Catalog.WebUI.Models
{
    public class TeamViewModel
    {
        public Team Team { get; set; }

        public int MemberCount { get; set; }
    }
}