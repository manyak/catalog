﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web.Http;
using Catalog.Core;
using Catalog.Core.Contracts;
using Catalog.DataContracts;

namespace Catalog.WebUI.ApiController
{
    public class TeamController : System.Web.Http.ApiController
    {
        private readonly ITeamDataService _teamDataService = TeamDataService.GetInstance();

        // GET api/<controller>
        public IEnumerable<Team> Get()
        {
            return _teamDataService.Teams;
        }

        // GET api/<controller>/5
        public Team Get(Guid id)
        {
            return _teamDataService.GetById(id);
        }

        // POST api/<controller>
        public void Post([FromBody]Team value)
        {
            _teamDataService.Add(value);
        }

        // PUT api/<controller>/5
        public void Put(Guid id, [FromBody]Team value)
        {
            var oldValue = _teamDataService.GetById(id);

            _teamDataService.Teams.Remove(oldValue);
            _teamDataService.Teams.Add(value);
        }

        // DELETE api/<controller>/5
        public void Delete(Guid id)
        {
            var oldValue = _teamDataService.GetById(id);

            _teamDataService.Teams.Remove(oldValue);
        }
    }
}