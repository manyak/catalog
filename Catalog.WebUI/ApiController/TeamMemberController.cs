﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Catalog.Core;
using Catalog.Core.Contracts;
using Catalog.DataContracts;

namespace Catalog.WebUI.ApiController
{
    public class TeamMemberController : System.Web.Http.ApiController
    {
        private readonly IPersonDataService _personDataService = PersonDataService.GetInstance();
        
        // GET api/<controller>
        public IEnumerable<Person> Get()
        {
            return _personDataService.People;
        }

        // GET api/<controller>/5
        public Person Get(Guid id)
        {
            var person = _personDataService.GetById(id);

            return person;
        }

        // POST api/<controller>
        public void Post([FromBody]Person value)
        {
            _personDataService.Add(value);
        }

        // PUT api/<controller>/5
        public void Put(Guid id, [FromBody]Person value)
        {
            var oldValue = _personDataService.GetById(id);

            _personDataService.People.Remove(oldValue);
            _personDataService.People.Add(value);
        }

        // DELETE api/<controller>/5
        public void Delete(Guid id)
        {
            var oldValue = _personDataService.GetById(id);

            _personDataService.People.Remove(oldValue);
        }
    }
}