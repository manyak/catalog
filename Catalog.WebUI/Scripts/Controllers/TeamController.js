﻿var app = angular.module('teamApp', []);

app.controller('teamController', function ($scope, $http) {
    $scope.firstname = "";

    $scope.init = function (teamInfo) {
        $scope.teamInfo = teamInfo;
    }

    $scope.submit = function () {
        var newTeamMember = {
            Name: $scope.newTeamMemberName,
            TeamId: $scope.teamInfo.Team.Id
        };

        $scope.teamInfo.TeamMembers.push(newTeamMember);

        $http({
            method: 'POST',
            url: '/api/TeamMember/',
            data: newTeamMember
        });
    };
});