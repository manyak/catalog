﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Catalog.Core;
using Catalog.Core.Contracts;
using Catalog.WebUI.Models;

namespace Catalog.WebUI.Controllers
{
    public class HomeController : Controller
    {
        private readonly ITeamDataService _teamDataService = TeamDataService.GetInstance();
        private readonly IPersonDataService _personDataService = PersonDataService.GetInstance();

        public ActionResult Index()
        {
            var teamViewModels = _teamDataService.Teams.Select(team => new TeamViewModel
            {
                Team = team,
                MemberCount = _personDataService.GetByTeamId(team.Id).Count()
            })
            .OrderByDescending(teamViewModel => teamViewModel.MemberCount);

            return View(teamViewModels);
        }

        public ActionResult TeamDetails(Guid id)
        {
            var teamMembersViewModel = new TeamDetailsViewModel
            {
                Team = _teamDataService.GetById(id),
                TeamMembers = _personDataService.GetByTeamId(id).OrderBy(person => person.Name)
            };

            return View(teamMembersViewModel);
        }
    }
}