﻿using Catalog.Core.Contracts;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Catalog.DataContracts;

namespace Catalog.Core
{
    // ToDo: add thread synchronization support.
    public class TeamDataService : ITeamDataService
    {
        private static readonly object Locker = new object();
        private static ITeamDataService _instance = null;

        private TeamDataService()
        {
            var eagles = new Team { Name = "Eagles" };
            var bears = new Team { Name = "Bears" };
            var monkeys = new Team { Name = "Monkeys" };

            Teams = new List<Team>
            {
                eagles,
                bears,
                monkeys
            };
        }

        public ICollection<Team> Teams { get; }

        public Team GetById(Guid id)
        {
            return Teams.FirstOrDefault(team => team.Id == id);
        }

        public void Add(Team team)
        {
            team.Id = Guid.NewGuid();
            Teams.Add(team);
        }

        public static ITeamDataService GetInstance()
        {
            if (_instance == null)
            {
                lock (Locker)
                {
                    if (_instance == null)
                    {
                        _instance = new TeamDataService();
                    }
                }
            }

            return _instance;
        }
    }
}
