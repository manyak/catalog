﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Catalog.Core.Contracts;
using Catalog.DataContracts;

namespace Catalog.Core
{
    // ToDo: add thread synchronization support.
    public class PersonDataService : IPersonDataService
    {
        private static readonly object Locker = new object();
        private static IPersonDataService _instance = null;

        private readonly ITeamDataService _teamDataService = TeamDataService.GetInstance();

        public PersonDataService()
        {
            var eagles = _teamDataService.Teams.First(team => team.Name == "Eagles");
            var bears = _teamDataService.Teams.First(team => team.Name == "Bears");
            var monkeys = _teamDataService.Teams.First(team => team.Name == "Monkeys");

            // Will not include Team property by default.
            // TODO: implement a lazy-loading mechanism in EF-fashion.
            People = new List<Person>
            {
                new Person { Name = "John", TeamId = monkeys.Id },
                new Person { Name = "Joe", TeamId = monkeys.Id },
                new Person { Name = "Ed", TeamId = eagles.Id },
                new Person { Name = "Merphy", TeamId = bears.Id },
                new Person { Name = "Met", TeamId = bears.Id }
            };
        }

        public ICollection<Person> People { get; }

        public Person GetById(Guid id)
        {
            return People.FirstOrDefault(person => person.Id == id);
        }

        public void Add(Person person)
        {
            person.Id = Guid.NewGuid();
            People.Add(person);
        }

        public IEnumerable<Person> GetByTeamId(Guid teamId)
        {
            return People.Where(person => person.TeamId == teamId);
        }

        public static IPersonDataService GetInstance()
        {
            if (_instance == null)
            {
                lock (Locker)
                {
                    if (_instance == null)
                    {
                        _instance = new PersonDataService();
                    }
                }
            }

            return _instance;
        }
    }
}
