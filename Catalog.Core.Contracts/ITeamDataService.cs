﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using Catalog.DataContracts;

namespace Catalog.Core.Contracts
{
    public interface ITeamDataService
    {
        ICollection<Team> Teams { get; }

        Team GetById(Guid id);

        void Add(Team team);
    }
}
