﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using Catalog.DataContracts;

namespace Catalog.Core.Contracts
{
    public interface IPersonDataService
    {
        ICollection<Person> People { get; }

        Person GetById(Guid id);

        void Add(Person person);

        IEnumerable<Person> GetByTeamId(Guid teamId);
    }
}
